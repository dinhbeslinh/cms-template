<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class AdminAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin:admins-api', ['except' => ['login', 'register']]);
    }

    public function register(Request $request)
    {
        $validEmail = Admin::WHERE([
            'email' => $request['email']
        ])->get();
        if (sizeof($validEmail)) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Email đã tồn tại',
                'data' => null
            ], 400);
        }
        $user = new Admin();
        $user['email'] = $request['email'];
        $user['name'] = $request['name'];
        $user['password'] = bcrypt($request['password']);
        $res = $user->save();
        if (!isset($res)) {
            return response()->json([
                'code_status' => 400,
                'message' => 'Failed',
                'data' => null
            ], 400);
        }
        return response()->json([
            'code_status' => 200,
            'message' => 'Success',
            'data' => $user
        ], 200);
    }

    public function login()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->guard('admins-api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json(auth()->guard('admins-api')->user());
    }

    public function logout()
    {
        auth()->guard('admins-api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->guard('admins-api')->refresh());
    }

    /**
     * @param string $token
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->guard('admins-api')->factory()->getTTL() * 60
        ]);
    }
}
