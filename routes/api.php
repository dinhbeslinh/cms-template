<?php

use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);

});
Route::group([
    'prefix' => 'auth',

], function ($router) {
    Route::post('registerAdmin', [AdminAuthController::class, 'register']);
    Route::post('loginAdmin', [AdminAuthController::class, 'login']);
});

Route::group([
    'prefix' => 'auth',
    'middleware' => 'admin:admins-api',

], function ($router) {
    Route::post('refreshAdmin', [AdminAuthController::class, 'refresh']);
    Route::post('logoutAdmin', [AdminAuthController::class, 'logout']);
    Route::post('meAdmin',[AdminAuthController::class,'me']);
});
